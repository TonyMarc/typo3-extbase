<?php

namespace Vendor\Project\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class LetterViewHelper
 *
 * @package Vendor\Project\ViewHelpers
 */
class LetterViewHelper extends AbstractViewHelper
{
    /**
     * Arguments Initialization
     */
    public function initializeArguments()
    {
        $this->registerArgument('string1', 'string',
            'First string.', true);
        $this->registerArgument('string2', 'string',
            'The second string.', true);
        $this->registerArgument('string3', 'string',
            'And the third string.', true);
    }

    /**
     * @param $iban string
     * @return string
     */
    public function render()
    {
        $stringOne = $this->arguments['string1'];
        $stringTwo = $this->arguments['string2'];
        $stringThree = $this->arguments['string3'];

        $formattedString = $stringOne . $stringTwo . $stringThree;
        return $formattedString;
    }
}