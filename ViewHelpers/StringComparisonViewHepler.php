<?php

namespace Vendor\Project\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class ComparisonViewHelper
 *
 * @package Vendor\Project\ViewHelpers
 */
class StringComparisonViewHepler extends AbstractViewHelper
{
    /**
     * @param $string string
     * @return int
     */
    public function render($string)
    {
        if(strcmp($string, 'String to be compared') === 0) {
            return 1;
        }
        if(strcmp($string, 'Another string to be compared') === 0) {
            return 2;
        }
        if(strcmp($string, 'Yet another string to be compared') === 0) {
            return 3;
        }
        return 0;
    }
}